const std = @import("std") ;

const Allocator = std.mem.Allocator ;
const StreamingParser = std.json.StreamingParser ;
const NonStreamingParser = std.json.Parser ;
const ValueTree = std.json.ValueTree ;
const Token = std.json.Token ;

/// This is the (streaming, a.k.a online) parser for the generated
/// extension_api.json file. Its main function is `parse`, during
/// which it accumulates some state (but not all, otherwise the
/// streaming approach would be pointless) and emits code at the
/// same time.
///
/// It uses a combination of streaming and non streaming parsers from
/// the std lib. Essentially the streaming part is meant to acquire
/// segments of the file that make one object, which the
/// non-streaming parser then parses to a tree.
///
/// S is the type of the output stream.
///
pub fn HybridParser(
    comptime S: type,
    comptime T: type,
    comptime map: anytype,
    comptime buffer_size: usize,
) type {
    return struct {
        /// Target stream.
        stdout: S,

        /// JSON streaming parser.
        streaming: StreamingParser,

        nonstreaming: NonStreamingParser,

        /// Buffer for the streaming parser.
        buffer: [buffer_size]u8,

        /// Current position in the buffer.
        cursor: usize,

        /// Current tokens,
        tokens: [2]?Token,

        /// Level of nested objects we are in at a given moment.
        nesting: u32,

        /// Callback to a function that will process tokens.
        process: TokenProcessor,

        /// Todo
        processObj: TokenProcessor,

        /// Segment of the buffer representing the current object of
        /// interest. This is preserved even when the buffer is read
        /// again — in fact that is the whole point of the parser.
        ///
        object_a: usize = 0,
        object_b: usize = 0,

        /// Type of the current(s) object(s) being discovered.
        object_type: T = .header,

        /// Todo
        object_pending: bool = false,

        const Self = @This() ;

        pub const Error =
            Allocator.Error || S.Error || StreamingParser.Error ||
            error { ObjectTooBig, InvalidCharacter, UnexpectedEndOfJson } ;

        pub const TokenProcessor = *const fn (*Self, *const Token) Error!void ;

        pub fn init(
            stdout: S,
            allocator: Allocator
        ) Self {
            var self = Self {
                .stdout = stdout,
                .streaming = StreamingParser.init(),
                .nonstreaming = NonStreamingParser.init(allocator, false),
                .buffer  = undefined,
                .cursor = 0,
                .tokens = .{null, null},
                .nesting = 0,
                .process = processToplevel,
                .processObj = undefined,
            } ;
            return self ;
        }

        pub fn deinit(self: *Self) void {
            self.nonstreaming.deinit() ;
        }

        /// Traverse the nodes of a JSON file. Because the API JSON file
        /// has a specific form, this parser uses it to its advantage.
        /// It will expect a high-level object whose entries are all
        /// sub-objects.
        ///
        pub fn parse(self: *Self, path: []const u8) !void {
            var overlap: usize = 0 ;
            var file = try std.fs.cwd().openFile(path, .{}) ;
            var reader = file.reader() ;
            defer file.close() ;
            while (reader.read(self.buffer[overlap..])) |len| {
                if (len == 0) {
                    break ;
                }
                // try self.stdout.print("Read {} bytes:\n---{s}---", .{len, self.buffer}) ;
                self.cursor = overlap ;
                for (self.buffer[overlap..overlap+len]) |char| {
                    try self.streaming.feed(char, &self.tokens[0], &self.tokens[1]) ;
                    for (self.tokens[0..]) |maybe_token| {
                        if (maybe_token) |token| {
                            // try self.stdout.print("{?}\n", .{token}) ;
                            self.process(self, &token) catch {
                            	try self.stdout.print("// token ignored...", .{}) ;
                            } ;
                        }
                    }
                    self.cursor += 1 ;
                }
                const former_overlap = overlap ;
                const former_a = self.object_a ;
                self.object_a = 0 ;
                if (self.object_pending) {
                    // This means that the object is too long for the
                    // current buffer. The current option is to just
                    // fail, but a TODO could be to just extend it.
                    if (former_a == 0) {
                        const ignore = true ;
                        if (ignore) {
                            self.object_pending = false ;
                            std.debug.print("Object ignored, too big for buffer.\n", .{}) ;
                        } else {
                            return Error.ObjectTooBig ;
                        }
                    }
                    overlap = overlap + len - former_a ;
                } else {
                    overlap = 32 ;
                }
                std.mem.copy(u8, self.buffer[0..overlap],
                	self.buffer[former_overlap+len-overlap..former_overlap+len]) ;
            } else |err| {
                if (err != @TypeOf(reader).Error.EndOfStream) {
                    return err ;
                } else {
                    unreachable ;
                }
            }
        }

        pub fn processToplevel(self: *Self, token: *const Token) Error!void {
            switch (token.*) {
                .String => |str| {
                    const s = str.slice(&self.buffer, self.cursor) ;
                    // try self.stdout.print("Trying to match token \"{s}\"\n", .{s}) ;
                    if (map.get(s)) |object_type| {
                        // try self.stdout.print("Change process\n", .{}) ;
                        self.object_type = object_type ;
                        self.process = self.processObj ; // InObject ;
                    }
                },
                else => {
                    // Nothing.
                }
            }
        }
    } ;
}
