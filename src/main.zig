const std    = @import("std") ;
const hybrid = @import("hybrid_parser.zig") ;

// const ArrayList = std.ArrayList ;
const Allocator = std.mem.Allocator ;
// const OutStream = std.fs.File.Writer ;
const OutStream = std.io.BufferedWriter(4096, std.fs.File.Writer).Writer ;
const ValueTree = std.json.ValueTree ;
const Value     = std.json.Value ;
const Token     = std.json.Token ;

const ObjectType = union(enum) {
    header,
    builtin_class_sizes,
    builtin_class_member_offsets,
    global_constants,
    global_enums,
    utility_functions,
    builtin_classes,
    classes,
    singletons,
    native_structures
} ;

const obj_types = std.ComptimeStringMap(ObjectType, .{
    .{"header", .header},
    .{"builtin_class_sizes", .builtin_class_sizes},
    .{"builtin_class_member_offsets", .builtin_class_member_offsets},
    .{"global_constants", .global_constants},
    .{"global_enums", .global_enums},
    .{"utility_functions", .utility_functions},
    .{"builtin_classes", .builtin_classes},
    .{"classes", .classes},
    .{"singletons", .singletons},
    .{"native_structures", .native_structures},
}) ;

const HybridParser = hybrid.HybridParser(OutStream, ObjectType, obj_types, 16*1024) ;

pub fn main() !void {
    // The generated code is emitted on stdout.
    const stdout_file = std.io.getStdOut().writer() ;
    var bw = std.io.bufferedWriter(stdout_file) ;
    const stdout = bw.writer() ;
    // const stdout = std.io.getStdOut().writer() ;
    var parser = HybridParser.init(stdout, std.heap.c_allocator) ;
    defer parser.deinit() ;
    // The following line is correct, but should be part of the
    // initialization function. But to circumvent a circular
    // dependency error (or compiler bug, I don't know), the
    // functions cannot accept TokenProcessor function ptrs.
    parser.processObj = processObj ;
    try parser.parse("res/extension_api.json") ;
    // try parser.parse("res/extension_api_sample.json") ;
    // parser.parse("res/extension_api_sample.json") catch |err| {
    // FIXME Find the correct error set...
    //     switch (err) {
    //         HybridParser.Error.ObjectTooBig => {
    //             std.debug.print("An object was too big for the parser.\n", .{}) ;
    //         },
    //         else => return err
    //     }
    // } ;
    try bw.flush() ;
}

fn processHeaderValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    const major = obj.get("version_major").?.Integer ;
    const minor = obj.get("version_minor").?.Integer ;
    const patch = obj.get("version_patch").?.Integer ;
    // const build = obj.get("version_build").?.String} ;
    // const fname = obj.get("version_full_name").?.String} ;
    // const status = obj.get("version_status").?.String} ;
    try self.stdout.print("// Generated bindings. Don't edit this file.\n", .{}) ;
    try self.stdout.print("// Godot {}.{}.{}\n",
        .{major, minor, patch}) ;
}

fn processBuiltinClassSizeValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processBuiltinClassMemberOffsetValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processGlobalConstantValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processGlobalEnumValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    const name = obj.get("name").?.String ;
    const vals = obj.get("values").?.Array ;
    // const is_bitfield = obj.get("is_bitfield").?.Bool ; // FIXME
    try self.stdout.print("pub const {s} = enum {{\n", .{name}) ;
    for (vals.items) |val| {
        const val_obj = val.Object ;
        const val_name = val_obj.get("name").?.String ;
        const val_value = val_obj.get("value").?.Integer ;
        try self.stdout.print("\t.{s} = {}\n", .{val_name, val_value}) ;
    }
    try self.stdout.print("}} ;\n", .{}) ;
}

fn processUtilityFunctionValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    const name = obj.get("name").?.String ;
    const is_vararg = obj.get("is_vararg").?.Bool ;
    const o_ret_type = obj.get("return_type") ;
    const o_args = obj.get("arguments") ;
    try self.stdout.print("pub fn {s}(", .{name}) ;
    if (o_args) |args| {
        for (args.Array.items) |arg| {
            const arg_obj = arg.Object ;
            const arg_name = arg_obj.get("name").?.String ;
            const arg_type = arg_obj.get("type").?.String ;
            try self.stdout.print("{s}: {s}, ", .{arg_name, arg_type}) ;
        }
    }
    if (is_vararg) {
        try self.stdout.print("...", .{}) ;
    }
    if (o_ret_type) |ret_type| {
        try self.stdout.print(") {s} ;\n", .{ret_type.String}) ;
    } else {
        try self.stdout.print(") void ; // NO_RETURN_TYPE\n", .{}) ;
    }

}

fn processBuiltinClassValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processClassValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processSingletonValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processNativeStructValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    const obj = vtree.root.Object ;
    _ = obj ;
    _ = self ;
}

fn processValueTree(
    self: *HybridParser,
    vtree: ValueTree,
) HybridParser.Error!void {
    // FIXME Replace with a table (maybe the compiler does it ?).
    return switch (self.object_type) {
        .header => processHeaderValueTree(self, vtree),
        .builtin_class_sizes => processBuiltinClassSizeValueTree(self, vtree),
        .builtin_class_member_offsets => processBuiltinClassMemberOffsetValueTree(self, vtree),
        .global_constants => processGlobalConstantValueTree(self, vtree),
        .global_enums => processGlobalEnumValueTree(self, vtree),
        .utility_functions => processUtilityFunctionValueTree(self, vtree),
        .builtin_classes => processBuiltinClassValueTree(self, vtree),
        .classes => processClassValueTree(self, vtree),
        .singletons => processSingletonValueTree(self, vtree),
        .native_structures => processNativeStructValueTree(self, vtree),
    } ;
}

fn processObj(
    self: *HybridParser,
    token: *const Token,
) HybridParser.Error!void {
    switch (token.*) {
        .ObjectBegin => {
            if (self.nesting == 0) {
                self.object_a = self.cursor ;
                self.object_pending = true ;
            }
            self.nesting += 1 ;
        },
        .ObjectEnd => {
            self.nesting = self.nesting -| 1 ;
            if (self.nesting == 0) {
                if (self.object_pending) {
                    self.object_b = self.cursor + 1 ;
                    const object = self.buffer[self.object_a..self.object_b] ;
                    const vtree = try self.nonstreaming.parse(object) ;
                    try processValueTree(self, vtree) ;
                    // try self.stdout.print("---------\n{s} (a: {}, b: {})\n",
                    //    .{object, self.object_a, self.object_b}) ;
                    self.nonstreaming.reset() ;
                    self.object_pending = false ;
                }
                switch (self.object_type) {
                    .header => self.process = HybridParser.processToplevel,
                    else => {}
                }
            }
        },
        .ArrayEnd => {
            // Leave the array if the token is the end of the
            // top level array containing objects of the type
            // we are trying to parse.
            const topmost = (self.nesting == 0) ;
            const correct = (self.object_type != ObjectType.header) ;
            if (topmost and correct) {
                self.process = HybridParser.processToplevel ;
            }
        },
        else => {}
    }
}
