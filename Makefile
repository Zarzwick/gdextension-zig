.PHONY: update

GODOT_EXECUTABLE = godot

all: update

exe: build.zig
	zig build

update:
	$(GODOT_EXECUTABLE) --dump-extension-api
	mkdir res
	mv extension_api.json res
	cp ../godot/core/extension/gdextension_interface.h res

clean:
	rm res/extension_api.json res/gdextension_interface.h

