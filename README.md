# Godot Binder generator for zig

**After two days of exploration and experimentation and I left the project
in this state. It will take substantial time and I don't need it so badly.
Feel free to reach me if you want to discuss about this, though.**

The core of the [documentation](https://twitter.com/reduzio/status/1507757962068205579?s=20) :> .
So essentially this project would be a binder generator, that emits a .zig file
that can be copied in the source tree of any extension you'd like to make in
zig.

```
make
```
